import torch
import torch.nn as nn
import torch.nn.functional as F

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, RichProgressBar
from pytorch_lightning.callbacks.progress.rich_progress import RichProgressBarTheme
from pytorch_lightning.loggers import WandbLogger

from torchvision import transforms

from dataset import CellDataset
from model.counting_model import CSRNet
from model.twobranch import countXplain


# import optuna stuff
import optuna
from optuna.integration import PyTorchLightningPruningCallback

import argparse

import time

pl.seed_everything(42)

def train():

    num_prototypes = 10
    lr = 0.1
    dataset = "VGG"

    hparams = {
        "num_prototypes": num_prototypes,
        "lr": lr,
        "bg_coef": 0,
        "fg_coef": 1,
        "diversity_coef": .8,
        "proto_to_feature_coef": 1,
        "data_coverage_coef": 0,
        "inter_class_coef": 0,
        "epsilon": 1e-4,  # Distance to similarity conversion
        "train_dir": f"../Datasets/{dataset}/trainval/images",
        "val_dir": f"../Datasets/{dataset}/test/images",
        "test_dir": f"../Datasets/{dataset}/test/images",
        "batch_size": 1,
        "num_workers": 4,
        
    }

    # Create the model
    ct_model = CSRNet().load_from_checkpoint(f"{dataset}_CSRNet.ckpt")
    print(f"Loaded the pretrained CSRNet model")

    proto_model = countXplain(hparams=hparams, count_model=ct_model)
    print(f"Created the ProtoModel")

    # prepare data
    proto_model.prepare_data()

    early_stop_callback = pl.callbacks.EarlyStopping(
        monitor="val_loss",
        min_delta=0.0,
        patience=200,
        check_finite=True,
        verbose=True,
        mode="min",
    )

    checkpoint_callback = ModelCheckpoint(
        monitor="val_loss",
        dirpath=f"FINAL_{dataset}_64_{num_prototypes}/",
        filename=f"{dataset}-trial-"
        + "-{epoch:02d}-{val_loss:.2f}",
        save_top_k=1,
        mode="min",
    )

    wandbLogger = WandbLogger(
         project=f"FINAL - countXplain - {dataset}"
    )

    trainer = pl.Trainer(
        accelerator="auto",
        max_epochs=200,
        # accumulate_grad_batches=8,
        callbacks=[
            RichProgressBar(),
            checkpoint_callback,
            early_stop_callback,
        ],
        logger=wandbLogger,
        # detect_anomaly=True,
        
    )

    # log the hyperparameters
    trainer.logger.log_hyperparams(hparams)

    # fit model
    trainer.fit(proto_model)

    # Load the best model
    proto_model = countXplain(hparams, ct_model).load_from_checkpoint(
        checkpoint_callback.best_model_path, count_model=ct_model
    )

    # Test the model
    trainer.test(proto_model)

    # return the test loss
    return trainer.callback_metrics["test_loss"].item()

def objective(trial, dataset, num_prototypes):
    # Optuna suggest hyperparameters
    lr = trial.suggest_loguniform("lr", 1e-9, 1e-2)

    hparams = {
        "num_prototypes": num_prototypes,
        "lr": lr,
        "bg_coef": 1,
        "fg_coef": 1,
        "diversity_coef": 1,
        "proto_to_feature_coef": 1,
        "data_coverage_coef": 1,
        "inter_class_coef": 1,
        "epsilon": 1e-4,  # Distance to similarity conversion
        "train_dir": f"../Datasets/{dataset}/trainval/images",
        "val_dir": f"../Datasets/{dataset}/test/images",
        "test_dir": f"../Datasets/{dataset}/test/images",
        "batch_size": 1,
        "num_workers": 4,
        
    }

    # Create the model
    ct_model = CSRNet().load_from_checkpoint(f"{dataset}_CSRNet.ckpt")
    print(f"Loaded the pretrained CSRNet model")

    proto_model = countXplain(hparams=hparams, count_model=ct_model)
    print(f"Created the ProtoModel")

    # prepare data
    proto_model.prepare_data()

    early_stop_callback = pl.callbacks.EarlyStopping(
        monitor="val_loss",
        min_delta=0.0,
        patience=100,
        check_finite=True,
        verbose=True,
        mode="min",
    )

    checkpoint_callback = ModelCheckpoint(
        monitor="val_loss",
        dirpath=f"checkpoints_{dataset}/",
        filename=f"{dataset}-trial-"
        + str(trial.number)
        + "-{epoch:02d}-{val_loss:.2f}",
        save_top_k=1,
        mode="min",
    )

    wandbLogger = WandbLogger(
        name=f"trial-{trial.number}", project=f"countXplain - {dataset}"
    )

    trainer = pl.Trainer(
        accelerator="auto",
        max_epochs=100,
        # accumulate_grad_batches=8,
        callbacks=[
            RichProgressBar(),
            checkpoint_callback,
            PyTorchLightningPruningCallback(trial, monitor="val_loss"),
            early_stop_callback,
        ],
        logger=wandbLogger,
        # detect_anomaly=True,
        
    )

    # log the hyperparameters
    trainer.logger.log_hyperparams(hparams)

    # fit model
    trainer.fit(proto_model)

    # Load the best model
    proto_model = countXplain(hparams, ct_model).load_from_checkpoint(
        checkpoint_callback.best_model_path, count_model=ct_model
    )

    # Test the model
    trainer.test(proto_model)

    # return the test loss
    return trainer.callback_metrics["test_loss"].item()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str, default="DCC")
    parser.add_argument("--num_prototypes", type=int, default=20)

    return parser.parse_args()


def main():
    args = parse_args()
    
    start_time = time.time()
    train()
    
    print(f"Time taken: {time.time() - start_time}")

if __name__ == "__main__":
    main()
