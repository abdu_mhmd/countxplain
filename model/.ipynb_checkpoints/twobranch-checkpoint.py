import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision import transforms

import pytorch_lightning as pl

from dataset import CellDataset
from .counting_model import CSRNet

class countXplain(pl.LightningModule):
    def __init__(self, hparams, count_model):
        super().__init__()
        self.save_hyperparameters(hparams)

        # The counting model will be frozen
        for param in count_model.parameters():
            param.requires_grad = False

        self.front_end = count_model.frontend
        self.back_end = count_model.backend
        self.output_layer = count_model.output_layer

        self.prototypes = nn.Parameter(torch.rand(self.hparams["num_prototypes"], 64, 1, 1), requires_grad=True)

        # Required for l2 convolution
        self.ones = nn.Parameter(torch.ones(self.prototypes.shape),
                                 requires_grad=False)
        
        self.add_on_layers = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=self.prototypes.shape[1], kernel_size=1),
            nn.Sigmoid()
        )


        self.counter = nn.Sequential(
            nn.Conv2d(self.prototypes.shape[0]//2, 1, kernel_size=1)
        )

        self.background = nn.Sequential(
            nn.Conv2d(self.prototypes.shape[0]//2, 1, kernel_size=1)
        )

        # for m in self.modules():
        #     if isinstance(m, nn.Conv2d):
        #         nn.init.normal_(m.weight, std=0.01)
        #         if m.bias is not None:
        #             nn.init.constant_(m.bias, 0)


        self.bg_coef = self.hparams["bg_coef"]
        self.fg_coef = self.hparams["fg_coef"]
        self.diversity_coef = self.hparams["diversity_coef"]
        self.proto_to_feature_coef =  self.hparams["proto_to_feature_coef"]
        self.data_coverage_coef = self.hparams["data_coverage_coef"]
        self.inter_class_coef = self.hparams["inter_class_coef"]
        

    def forward(self, x):
        # Get feaures from the front end
        x = self.front_end(x)
        x = self.back_end(x)
        x = self.add_on_layers(x)

        # Get the distance between the features and the prototypes
        distances = self._l2_convolution(x)

        # convert the distance to similarity
        similarity = self.distance2similarity(distances)


        # The first half of the similarity scores will be passed through the counter
        fg = self.counter(similarity[:, :self.prototypes.shape[0]//2, :, :])
        # fg = torch.mean(similarity[:, :self.prototypes.shape[0]//2, :, :], dim=1, keepdim=True)

        # The second half of the similarity scores will be passed through the background layers
        bg = self.background(similarity[:, self.prototypes.shape[0]//2:, :, :])
        # bg = torch.mean(similarity[:, self.prototypes.shape[0]//2:, :, :], dim=1, keepdim=True)

        
        return x,bg, fg, distances
    
    def diversity_loss(self):
        '''
        A method to calculate the diversity loss

        Returns:
            The diversity loss
        '''

        num_prototypes, _,_,_ = self.prototypes.shape

        prototypes = self.prototypes.view(num_prototypes, -1)
        norms = torch.norm(prototypes, dim=1,p=2, keepdim=True)
        prototypes = prototypes / norms

        dot_product = torch.mm(prototypes, prototypes.t())

        # mask out the diagonal because of self similarity
        mask = torch.ones_like(dot_product) - torch.eye(num_prototypes, device=dot_product.device)
        dot_product = dot_product * mask

        # Calculate the diversity loss
        diversity_loss = torch.sum(torch.abs(dot_product))

        return diversity_loss

    def distance2similarity(self, distance):
        '''
        A method to convert the distance to similarity

        Args:
            distance: The distance between the features and the prototypes

        Returns:
            The similarity between the features and the prototypes
        '''

        similarity = torch.log((distance + 1) / (distance + self.hparams["epsilon"]))
        return similarity
    
        # Method by InsightRNet
        # dist_max = self.prototypes.shape[1] * \
        #         self.prototypes.shape[2] * self.prototypes.shape[3]
        # return 1 / ((distance / dist_max) + self.hparams["epsilon"])

    def calculate_similarity(self, x):
        '''
        A method to measure the similarity with fixed inner product. (Adapted from https://github.com/cvlab-stonybrook/zero-shot-counting/blob/main/models/matcher.py)

        Args:
            x: The features from the front end

        Returns:
            The similarity between the features and the prototypes
        '''

        # Get the features shape
        bs, c, h, w = x.shape

        # Reshape the features to be of shape (bs, hw, c)
        features = x.flatten(2).permute(0, 2, 1)

        # Reshape the prototypes of shape (num_prototypes, c, hw)
        prototypes = self.prototypes.flatten(2)

        # Calculate the similarity between the features and the prototypes
        similarity = torch.bmm(features, prototypes)

    def _l2_convolution(self,x):
        '''
        A method to apply self.prototype vectors as L2 convolution filters on input x

        Args:
            x: The features from the front end

        Returns:
            The similarity between the features and the prototypes
        '''
        # print(f"Prototypes shape: {self.prototypes.shape}")
        # print(f"X shape: {x.shape}")
        # print(F"ones shape: {self.ones.shape}")


        # Get x^2
        x2 = x**2
        x2_patch_sum = F.conv2d(x2, weight=self.ones)

        protos = self.prototypes

        p2 = protos**2

        p2 = torch.sum(p2, dim=(1,2,3))

        p2_reshape = p2.view(-1, 1, 1)

        xp = F.conv2d(x, weight=protos)

        intermediate = -2*xp + p2_reshape

        distances = F.relu(x2_patch_sum + intermediate)

        return distances

    # def proto_to_feature_loss(self,distances):
    #     '''
    #     A method to calculate the prototype to feature loss. It will find the minimum distance between the prototypes and the points in the feature map. This is will ensure that the prototypes are close to atleast one point in the feature map.

    #     L = 1/m * sum(min(||p_m - x_n_i||^2))

    #     Args:
    #         distances: The distance from each prototype to each point in the feature map

    #     Returns:
    #         The prototype to feature loss
    #     '''
    #     # Find the minimum distance from each prototype to each point in the feature map
    #     bs,num_prototypes,h,w = distances.shape

    #     min_distance, _ = torch.min(distances.view(bs, num_prototypes, -1), dim=2)

    #     # Get the mean of the minimum distance
    #     mean_min_distance = torch.mean(min_distance)

    #     return mean_min_distance

    def data_coverage_loss(self,distances):
        '''
        A method to calculate the data coverage loss. It will find the minimum distance between the points in the feature map and the prototypes. This will ensure the points in the feature map are clustered around the prototypes.

        L = 1/N * sum(min(||x_n_i - p_m||^2))

        Args:
            distances: The distance from each prototype to each point in the feature map
        Returns:
            The data coverage loss
        '''
        
        # Find the minimum distance from each point in the feature map to each prototype
        min_distance, _ = torch.min(distances, dim=1)

        # Get the mean of the minimum distance
        mean_min_distance = torch.mean(min_distance)

        return mean_min_distance

    def orthonormality_loss(self):
        '''
        A method to calculate the orthonormality loss. It will calculate the dot product between the prototypes and ensure that they are orthogonal.

        L = sum(|p_m * p_n|)

        Returns:
            The orthonormality loss
        '''
        s_loss = 0

        for k in range(2):
            # Get the first half of the prototypes
            p_k = self.prototypes[k*self.prototypes.shape[0]//2:(k+1)*self.prototypes.shape[0]//2, :, :, :]
            
            # Get the mean of the prototypes
            p_k_mean = torch.mean(p_k, dim=0)

            # Normalize the prototypes
            p_k_2 = p_k - p_k_mean

            # Squeeze the prototypes
            p_k_2 = p_k_2.squeeze().squeeze()

            # Calculate the dot product between the prototypes
            p_k_dot = p_k_2 @ p_k_2.T

            # Mask the diagonal
            s_matrix = p_k_dot - (torch.eye(p_k.shape[0], device=p_k_dot.device))

            # Calculate the orthonormality loss
            s_loss += torch.norm(s_matrix, p=2)

        return s_loss/2

    def inter_class_loss(self):


        # prototypes need to be reshaped to (num_prototypes, d*h*w)
        flattened_prototypes = self.prototypes.view(self.prototypes.shape[0], -1)

        # Getting the foreground and background prototypes
        fg_prototypes = flattened_prototypes[:self.prototypes.shape[0] // 2, :]
        bg_prototypes = flattened_prototypes[self.prototypes.shape[0] // 2:, :]

        fg_centroid = torch.mean(fg_prototypes, dim=0)
        bg_centroid = torch.mean(bg_prototypes, dim=0)

        # Calculate the inter class loss
        inter_class_loss = torch.norm(fg_centroid - bg_centroid, p=2)

        return inter_class_loss

    def proto_to_feature_loss(self,fmaps,distances):
        '''
        A modified version of the proto_to_feature loss.

        '''
        loss = 0
        batch_size, num_prototypes, h, w = distances.shape

        single_channel_fmaps = torch.mean(fmaps, dim=1, keepdim=True)

        flattened_fmaps = single_channel_fmaps.view(batch_size, -1) 
        flattened_distances = distances.view(batch_size, num_prototypes, -1)

        for i in range(num_prototypes):
            if i < num_prototypes//2:

                max_indices = torch.argmax(flattened_fmaps, dim=1)

                dist = flattened_distances[range(batch_size), i, max_indices]

            else:
                min_indices = torch.argmin(flattened_fmaps, dim=1)

                dist = flattened_distances[range(batch_size), i, min_indices]

            loss += torch.mean(dist)

        return loss/num_prototypes


    def training_step(self, batch, batch_idx):
        x, y = batch

        # get the inverted density map
        y_inv = torch.exp(-1 * y)

        # FWD pass
        fmaps, bg,fg, distances = self(x)

        # calculate the losses
        # Counting loss
        bg_loss = F.mse_loss(bg, y_inv, reduction='sum')
        fg_loss = F.mse_loss(fg, y, reduction='sum')

        # Diversity loss
        diversity_loss = self.orthonormality_loss()
        
        # Prototype to feature loss
        proto_to_feature_loss = self.proto_to_feature_loss(fmaps,distances)

        # Data coverage loss
        data_coverage_loss = self.data_coverage_loss(distances)

        # Intra class loss
        inter_class = self.inter_class_loss()
        
        loss = self.bg_coef * bg_loss + self.fg_coef * fg_loss + self.diversity_coef * diversity_loss + self.proto_to_feature_coef * \
            proto_to_feature_loss + self.data_coverage_coef * \
            data_coverage_loss + self.inter_class_coef * inter_class

        # Show a warning if any of the losses are NaN
        if torch.isnan(loss):
            print(f"Loss is NaN")

            # Check if dmap contains NaN values
            print(f"Checking dmap for NaN values")
            print(f"Number of NaN values in dmap: {torch.sum(torch.isnan(bg))}")
            print(f"Number of NaN values in dmap: {torch.sum(torch.isnan(fg))}")
            print(f"Number of NaN values in y: {torch.sum(torch.isnan(y))}")
    

        self.log('train_loss', loss)
        self.log('train_bg_loss', bg_loss)
        self.log('train_fg_loss', fg_loss)
        self.log('train_diversity_loss', diversity_loss)
        self.log('train_proto_to_feature_loss', proto_to_feature_loss)
        self.log('train_data_coverage_loss', data_coverage_loss)
        self.log('train_inter_class_loss', inter_class)

        return loss
    
    def validation_step(self, batch, batch_idx):
        x, y = batch

        # get the inverted density map
        y_inv = torch.exp(-1 * y)

        # Fwd pass
        fmaps,bg,fg, distances = self(x)

        # calculate the losses
        # Counting loss
        bg_loss = F.mse_loss(bg, y_inv, reduction='sum')
        fg_loss = F.mse_loss(fg, y, reduction='sum')
    
        # Diversity loss
        diversity_loss = self.orthonormality_loss()

        # Prototype to feature loss
        proto_to_feature_loss = self.proto_to_feature_loss(fmaps,distances)

        # Data coverage loss
        data_coverage_loss = self.data_coverage_loss(distances)

        inter_class = self.inter_class_loss()

        # loss = self.delta * bg_loss + self.alpha * fg_loss + self.beta * diversity_loss + self.gamma * proto_to_feature_loss + self.delta * data_coverage_loss + self.beta * inter_class

        loss = self.bg_coef * bg_loss + self.fg_coef * fg_loss + self.diversity_coef * diversity_loss + self.proto_to_feature_coef * \
            proto_to_feature_loss + self.data_coverage_coef * \
            data_coverage_loss + self.inter_class_coef * inter_class


        self.log('val_loss', loss)
        self.log('val_bg_loss', bg_loss)
        self.log('val_fg_loss', fg_loss)
        self.log('val_diversity_loss', diversity_loss)
        self.log('val_proto_to_feature_loss', proto_to_feature_loss)
        self.log('val_data_coverage_loss', data_coverage_loss)
        self.log('val_inter_class_loss', inter_class)

        return loss
    
    def test_step(self, batch, batch_idx):
        x, y = batch

        # get the inverted density map
        y_inv = torch.exp(-1 * y)

        # Fwd pass
        fmaps,bg,fg, distances = self(x)

        # calculate the losses
        # Counting loss
        bg_loss = F.mse_loss(bg, y_inv, reduction='sum')
        fg_loss = F.mse_loss(fg, y, reduction='sum')

        # Diversity loss
        diversity_loss = self.orthonormality_loss()

        # # Prototype to feature loss
        proto_to_feature_loss = self.proto_to_feature_loss(fmaps,distances)

        # # Data coverage loss
        data_coverage_loss = self.data_coverage_loss(distances)

        inter_class = self.inter_class_loss()

        # loss = self.delta * bg_loss + self.alpha * fg_loss + self.beta * diversity_loss + self.gamma * proto_to_feature_loss + self.delta * data_coverage_loss + self.beta * inter_class
        loss = self.bg_coef * bg_loss + self.fg_coef * fg_loss + self.diversity_coef * diversity_loss + self.proto_to_feature_coef * \
            proto_to_feature_loss + self.data_coverage_coef * \
            data_coverage_loss + self.inter_class_coef * inter_class

        self.log('test_loss', loss)
        self.log('test_bg_loss', bg_loss)
        self.log('test_fg_loss', fg_loss)
        self.log('test_diversity_loss', diversity_loss)
        self.log('test_proto_to_feature_loss', proto_to_feature_loss)
        self.log('test_data_coverage_loss', data_coverage_loss)
        self.log('test_inter_class_loss', inter_class)


        return loss
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.hparams["lr"])
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size = 30, gamma = .1)
        
        return [optimizer],[scheduler]
    
    def prepare_data(self):
        self.train_dataset = CellDataset(self.hparams["train_dir"], transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                             std=[0.229, 0.224, 0.225])
    ]))
        self.val_dataset = CellDataset(self.hparams["val_dir"], transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                             std=[0.229, 0.224, 0.225])
    ]))
        self.test_dataset = CellDataset(self.hparams["test_dir"], transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                             std=[0.229, 0.224, 0.225])
    ]))

    def train_dataloader(self):
        return torch.utils.data.DataLoader(self.train_dataset, batch_size=self.hparams["batch_size"], shuffle=True, num_workers=self.hparams["num_workers"])
    
    def val_dataloader(self):
        return torch.utils.data.DataLoader(self.val_dataset, batch_size=self.hparams["batch_size"], shuffle=False, num_workers=self.hparams["num_workers"])
    
    def test_dataloader(self):
        return torch.utils.data.DataLoader(self.test_dataset, batch_size=self.hparams["batch_size"], shuffle=False, num_workers=self.hparams["num_workers"])
    