# CountXplain

This repository contains the code for CountXplain, a method for explaining cell counting models using concept-based explanations. 

## Requirements

The code is written in Python 3.8.17 and uses PyTorch 1.3.13. The required packages can be installed using the requirements.txt file.

## Dataset

The dataset used in this study can be downloaded from [here](https://github.com/markmarsden/DublinCellDataset)
We used a preprocessed version of the DCC dataset from [here](https://drive.google.com/drive/folders/1Ap91365akA1FkuWLv9k_DHt_EtrFlrbY?usp=sharing). The final preprocessed version can be found [here](https://drive.google.com/drive/folders/1Ap91365akA1FkuWLv9k_DHt_EtrFlrbY?usp=sharing)

The list of images in training and testing sets are same as the ones used in [1].


## Training

To run the explainability method, first a model needs to be trained. This can be done using the main_trainer.py script. This script can be called using:
```python
    python train.py
```
Once the density map estimation model is trained, the explainability method can be run using:
```python
    python train_dcc.py
```

All the hyperparameter settings are defined in the `Config.py` file. 


## Evaluation
To run evaluation, the following script can be used:
```python
    python eval.py --counter <counter> --explainer <explainer> --data_path <data_path> --save_dir <save_dir> 
```
where `counter` is the path to the trained density map estimation model, `explainer` is the path to the trained explainability model, `data_path` is the path to the dataset, and `save_dir` is the path to the directory where the results will be saved.

## References
[1] Marsden, M., McGuinness, K., Little, S., Keogh, C. E., & E., N. (2017). People, Penguins and Petri Dishes: Adapting Object Counting Models To New Visual Domains And Object Types Without Forgetting. ArXiv. /abs/1711.05586





